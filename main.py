import sqlite3
from lxml import etree
from http.server import HTTPServer, CGIHTTPRequestHandler

from sql_constants import *
from xml_utils import dump_products, dump_services, dump_sale_products, dump_sale_services


def dump_database_to_xml():
    """Выгрузить базу данных в XML файл"""

    root = etree.Element('root')  # Корень файла

    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    dump_products(cursor, root)
    dump_services(cursor, root)
    dump_sale_products(cursor, root)
    dump_sale_services(cursor, root)

    et = etree.ElementTree(root)
    et.write('database_dump.xml', pretty_print=True)


def create_database():
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    try:
        cursor.execute(SQL_CREATE_PRODUCT)
        cursor.execute(SQL_CREATE_SERVICE)
        cursor.execute(SQL_CREATE_SALE_PRODUCT)
        cursor.execute(SQL_CREATE_SALE_SERVICE)
    except sqlite3.OperationalError as e:
        print(str(e))


if __name__ == '__main__':
    create_database()
    dump_database_to_xml()
    server_address = ('', 8000)
    httpd = HTTPServer(server_address, CGIHTTPRequestHandler)
    print('Server started on %s:%s' % httpd.server_address)
    httpd.serve_forever()
