from lxml import etree


def dump_products(cursor, root):
    """Выгружает в root таблицу product"""

    products = etree.SubElement(root, 'products')

    cursor.execute('SELECT * FROM product')
    product_table = cursor.fetchall()

    for row in product_table:
        product_id, name, price = row

        products_item = etree.SubElement(products, 'item')

        products_item_id = etree.SubElement(products_item, 'id')
        products_item_id.text = str(product_id)

        products_item_name = etree.SubElement(products_item, 'name')
        products_item_name.text = name

        products_item_price = etree.SubElement(products_item, 'price')
        products_item_price.text = str(price)


def dump_services(cursor, root):
    """Выгружает в root таблицу service"""

    services = etree.SubElement(root, 'services')

    cursor.execute('SELECT * FROM service')
    service_table = cursor.fetchall()

    for row in service_table:
        service_id, name, price = row

        services_item = etree.SubElement(services, 'item')

        services_item_id = etree.SubElement(services_item, 'id')
        services_item_id.text = str(service_id)

        services_item_name = etree.SubElement(services_item, 'name')
        services_item_name.text = name

        services_item_price = etree.SubElement(services_item, 'price')
        services_item_price.text = str(price)


def dump_sale_products(cursor, root):
    """Выгружает в root таблицу sale_product"""

    sale_products = etree.SubElement(root, 'sale_products')

    cursor.execute('SELECT * FROM sale_product')
    sale_product_table = cursor.fetchall()

    for row in sale_product_table:
        sale_id, amount, product_id = row

        sale_products_item = etree.SubElement(sale_products, 'item')

        sale_products_item_id = etree.SubElement(sale_products_item, 'id')
        sale_products_item_id.text = str(sale_id)

        sale_products_item_amount = etree.SubElement(sale_products_item, 'amount')
        sale_products_item_amount.text = str(amount)

        sale_products_item_product_id = etree.SubElement(sale_products_item, 'product_id')
        sale_products_item_product_id.text = str(product_id)


def dump_sale_services(cursor, root):
    """Выгружает в root таблицу sale_service"""

    sale_services = etree.SubElement(root, 'sale_services')

    cursor.execute('SELECT * FROM sale_service')
    sale_service_table = cursor.fetchall()

    for row in sale_service_table:
        sale_id, amount, service_id = row

        sale_services_item = etree.SubElement(sale_services, 'item')

        sale_services_item_id = etree.SubElement(sale_services_item, 'id')
        sale_services_item_id.text = str(sale_id)

        sale_services_item_amount = etree.SubElement(sale_services_item, 'amount')
        sale_services_item_amount.text = str(amount)

        sale_services_item_product_id = etree.SubElement(sale_services_item, 'service_id')
        sale_services_item_product_id.text = str(service_id)
