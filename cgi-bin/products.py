#!/usr/bin/env python3
import cgi
import sqlite3

from sql_constants import DB_PATH

form = cgi.FieldStorage()
product_id = form.getfirst('id', '')
name = form.getfirst('name', '')
price = form.getfirst('price', '')

header = 'Content-type: text/html\n'
html = """<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Обработка данных форм</title>
</head>
<body>
    <h1>Продукт создан!</h1>
"""

print(header)
print(html)

print('<p>ID: {}</p>'.format(product_id))
print('<p>Название продукта: {}</p>'.format(name))
print('<p>Цена за еденицу: {}</p>'.format(price))

print("""</body></html>""")

conn = sqlite3.connect(DB_PATH)
cursor = conn.cursor()
cursor.execute('insert into product values(?, ?, ?)', (product_id, name, price))
conn.commit()
