#!/usr/bin/env python3
import cgi
import sqlite3

from sql_constants import DB_PATH

form = cgi.FieldStorage()
sale_product_id = form.getfirst('id', '')
amount = form.getfirst('amount', '')
product = form.getfirst('product', '')

header = 'Content-type: text/html\n'
html = """<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Обработка данных форм</title>
</head>
<body>
    <h1>Продажа записана в базе!</h1>
"""

print(header)
print(html)

print('<p>ID: {}</p>'.format(sale_product_id))
print('<p>ID продукта: {}</p>'.format(product))
print('<p>Количество: {}</p>'.format(amount))

print("""</body></html>""")

conn = sqlite3.connect(DB_PATH)
cursor = conn.cursor()
cursor.execute('insert into sale_product values(?, ?, ?)', (sale_product_id, amount, product))
conn.commit()
