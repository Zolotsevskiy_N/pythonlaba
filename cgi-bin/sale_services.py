#!/usr/bin/env python3
import cgi
import sqlite3

from sql_constants import DB_PATH

form = cgi.FieldStorage()
sale_service_id = form.getfirst('id', '')
amount = form.getfirst('amount', '')
service = form.getfirst('service', '')

header = 'Content-type: text/html\n'
html = """<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Обработка данных форм</title>
</head>
<body>
    <h1>Услуга записана в базе!</h1>
"""

print(header)
print(html)

print('<p>ID: {}</p>'.format(sale_service_id))
print('<p>ID услуги: {}</p>'.format(service))
print('<p>Количество: {}</p>'.format(amount))

print("""</body></html>""")

conn = sqlite3.connect(DB_PATH)
cursor = conn.cursor()
cursor.execute('insert into sale_service values(?, ?, ?)', (sale_service_id, amount, service))
conn.commit()
