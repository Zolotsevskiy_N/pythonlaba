#!/usr/bin/env python3
import sqlite3

from sql_constants import DB_PATH

header = 'Content-type: text/html\n'
html = """<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Обработка данных форм</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body class="container pt-2 text-light bg-dark">
    <h1 class="mb-4">Продукты</h1>
    <table class="table table-striped table-dark table-hover table-bordered">
        <thead>
            <tr><th>ID</th><th>Название продукта</th><th>Цена</th>
        </thead>
        <tbody>
            {product}
        </tbody>
    </table>
    <h1 class="mb-4">Услуги</h1>
    <table class="table table-striped table-dark table-hover table-bordered">
        <tr><th>ID</th><th>Название услуги</th><th>Цена</th>
        {service}
    </table>
    <h1 class="mb-4">Продажи продукта</h1>
    <table class="table table-striped table-dark table-hover table-bordered">
        <tr><th>ID продажи</th><th>Количество</th><th>ID продукта</th>
        {sale_products}
    </table>
    <h1 class="mb-4">Продажи услуги</h1>
    <table class="table table-striped table-dark table-hover table-bordered">
        <tr><th>ID продажи</th><th>Количество</th><th>ID услуги</th>
        {sale_services}
    </table>
</body>
</html>
"""

conn = sqlite3.connect(DB_PATH)
cursor = conn.cursor()

cursor.execute('select * from product')
products = cursor.fetchall()
cursor.execute('select * from service')
services = cursor.fetchall()
cursor.execute('select * from sale_product')
sale_products = cursor.fetchall()
cursor.execute('select * from sale_service')
sale_services = cursor.fetchall()

table_products = ''
for row in products:
    table_products += '<tr>'
    for td in row:
        table_products += '<td>' + str(td) + '</td>'
    table_products += '</tr>'

table_services = ''
for row in services:
    table_services += '<tr>'
    for td in row:
        table_services += '<td>' + str(td) + '</td>'
    table_services += '</tr>'

table_sale_products = ''
for row in sale_products:
    table_sale_products += '<tr>'
    for td in row:
        table_sale_products += '<td>' + str(td) + '</td>'
    table_sale_products += '</tr>'

table_sale_services = ''
for row in sale_products:
    table_sale_services += '<tr>'
    for td in row:
        table_sale_services += '<td>' + str(td) + '</td>'
    table_sale_services += '</tr>'

print(header)
print(html.format(
    product=table_products,
    service=table_services,
    sale_products=table_sale_products,
    sale_services=table_sale_services,
))
