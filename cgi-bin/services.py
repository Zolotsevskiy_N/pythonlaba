#!/usr/bin/env python3
import cgi
import sqlite3

from sql_constants import DB_PATH

form = cgi.FieldStorage()
service_id = form.getfirst('id', '')
name = form.getfirst('name', '')
price = form.getfirst('price', '')

header = 'Content-type: text/html\n'
html = """<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Обработка данных форм</title>
</head>
<body>
    <h1>Услуга создана!</h1>
"""

print(header)
print(html)

print('<p>ID: {}</p>'.format(service_id))
print('<p>Название услуги: {}</p>'.format(name))
print('<p>Цена за еденицу: {}</p>'.format(price))

print("""</body></html>""")

conn = sqlite3.connect(DB_PATH)
cursor = conn.cursor()
cursor.execute('insert into service values(?, ?, ?)', (service_id, name, price))
conn.commit()
