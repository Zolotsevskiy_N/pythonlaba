import os
from pathlib import Path

SQL_CREATE_PRODUCT = """
create table product (
id integer, 
name text, 
price integer, 
primary key (id)
);"""

SQL_CREATE_SERVICE = """
create table service (
id integer,
name text,
price integer,
primary key(id)
);"""

SQL_CREATE_SALE_SERVICE = """
create table sale_service (
id integer,  
amount integer,
service integer, 
primary key(id),
foreign key (service) references service(id)
);"""

SQL_CREATE_SALE_PRODUCT = """
create table sale_product (
id integer,
amount integer,
product integer,
primary key(id),
foreign key (product) references product(id)
);"""

CURRENT_DIR = Path().absolute()
DB_PATH = os.path.join(CURRENT_DIR, 'database.db')
